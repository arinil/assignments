/*
Nama        : Arinil Haq
NPM         : 2006597430
Tanggal     : 11 Maret 2021
Desc        : Program ini akan melakukan validasi dan ekstaksi informasi melalui data NPM yang diinput. 
*/
package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        // Mengubah npm dari long menjadi string
        String npmString = String.valueOf(npm);
        String tahunLahir = npmString.substring(8, 12);
        int umur = getTahunMasuk(npm) - Integer.parseInt(tahunLahir);
        // Mengecek validate condition dan mengembalikkan true jika semua condition terpenuhi, dan false untuk sebaliknya
        if ((getTahunMasuk(npm) >= 2010) && npmString.length() == 14 && kodeJurusan(npm) && umur >= 15 
        && cekKodeNpm(npm) && cekTanggalLahir(npm) && cekKodeNpm(npm))  {
            return true;
        } else {
            return false;
        }
    }

    public static String extract(long npm) {
        String npmString = String.valueOf(npm);

        // Menginisiasi variable tahunMasuk dengan memanggil method getTahunMasuk
        String tahunMasuk = "Tahun masuk: " + String.valueOf(getTahunMasuk(npm));
        // Menginisiasi variable jurusan dengan memanggil method getJurusan
        String jurusan = "Jurusan: " + getJurusan(npm);
        //Menginisiasi variable tanggalLahir dengan substring
        String tanggalLahir = "Tanggal Lahir: " + npmString.substring(4,6) + "-" + npmString.substring(6,8) + "-" + npmString.substring(8,12);

        // Jika method validate bernilai true, maka akan dikembalikkan tahunMasuk, jurusan, dan tanggalLahir
        if (validate(npm)) {
            return tahunMasuk + '\n' + jurusan + '\n' + tanggalLahir;
        } 
        // Jika method validate bernilai false, maka akan dikembalikkan "NPM tidak valid!"
        else {
            return "NPM tidak valid!";
        }
    }

    public static int getTahunMasuk(long npm) {
        String npmString = String.valueOf(npm);
        // Menginisisasi tahun masuk dengan substring
        String tahunMasuk = "20" + npmString.substring(0,2);  
        int tahunMasukInt = Integer.parseInt(tahunMasuk);
        // Mengembalikkan tahunMasuk dalam integer
        return tahunMasukInt;
    }

    public static boolean kodeJurusan(long npm) {
        String npmString = String.valueOf(npm);
        // Menginisiasi variable kodeJurusanNpm dalam string
        String kodeJurusanNpm = npmString.substring(2,4);
        // Mengubah variable ke dalam integer
        int kodeJurusanNpmInt = Integer.parseInt(kodeJurusanNpm);
        // Mengecek apakah kode jurusan ada pada daftar kode jurusan
        if (kodeJurusanNpmInt == 01 || kodeJurusanNpmInt == 02 || kodeJurusanNpmInt == 03 || 
        kodeJurusanNpmInt == 11 || kodeJurusanNpmInt == 12 ) {
            return true;
        } else {
            return false;
        }
    }
    
    public static String getJurusan(long npm) {
        String npmString = String.valueOf(npm);
        // Menginisiasi variable kodeJurusanNpm dalam string
        String kodeJurusanNpm = npmString.substring(2,4);
        // Mengubah variable ke dalam integer
        int kodeJurusanNpmInt = Integer.parseInt(kodeJurusanNpm);
        // Mengembalikkan nama jurusan sesuai dengan kode jurusan
        if (kodeJurusanNpmInt == 01) {
            return "Ilmu Komputer";
        } 
        else if (kodeJurusanNpmInt == 02) {
            return "Sistem Informasi";
        }
        else if (kodeJurusanNpmInt == 03) {
            return "Teknologi Informasi";
        }
        else if (kodeJurusanNpmInt == 11) {
            return "Teknik Telekomunikasi";
        }
        else {
            return "Teknik Elektro";
        }
    }

    public static boolean cekTanggalLahir(long npm) {
        String npmString = String.valueOf(npm);

        // Menginisiasi variable tanggal, bulan, dan tahun dalam integer
        int tanggal = Integer.parseInt(npmString.substring(4,6));
        int bulan = Integer.parseInt(npmString.substring(6,8));
        int tahun = Integer.parseInt(npmString.substring(8,12));
        // Menginisiasi variable kabisat dalam float
        float kabisat = tahun%4;

        // Mengecek kebenaran tanggal dan bulan
        if (bulan == 1 && tanggal <= 31) {
            return true;
        }
        else {
            if ((bulan == 2) && (tanggal <= 28) && (kabisat != 0)) {
                return true;
            }
            else if ((bulan == 3) && (tanggal <= 29) && (kabisat == 0)) {
                return true;
            }
            else if ((bulan == 4) && (tanggal <= 30)) {
                return true;
            }
            else if ((bulan == 5) && (tanggal <= 31)) {
                return true;
            }
            else if ((bulan == 6) && (tanggal <= 30)) {
                return true;
            }
            else if ((bulan == 7) && (tanggal <= 31)) {
                return true;
            }
            else if ((bulan == 8) && (tanggal <= 31)) {
                return true;
            }
            else if ((bulan == 9) && (tanggal <= 30)) {
                return true;
            }
            else if ((bulan == 10) && (tanggal <= 31)) {
                return true;
            }
            else if ((bulan == 11) && (tanggal <= 30)) {
                return true;
            }
            else if ((bulan == 12) && (tanggal <= 31)) {
                return true;
            }
            else {
                return false;
            }
        }   
    }

    public static boolean cekKodeNpm(long npm) {
        String npmString = String.valueOf(npm);
        // Menginisiasi variable kosong
        int firstDigit, lastDigit;
        int akumulasi = 0;
        for (int i = 0; i <= ((npmString.length()-1) / 2); i++) {
            // Mengassign variable firstDigit dengan substring (dari depan)
            firstDigit = Integer.parseInt(npmString.substring(i,i+1));
            // Mengassign variable lastDigit dengan substring (dari belakang)
            lastDigit = Integer.parseInt(npmString.substring(npmString.length()-2-i, npmString.length()-1-i));
            // Mengakumulasi hasil kali firstDigit dan lastDigit
            akumulasi += (firstDigit * lastDigit);
        }

        // Menginisiasi variable kosong dengan nama hasil
        int hasil = 0;
        // Jika hasil akumulasi kurang dari sama dengan 10 hasil akan diassign dengan nilai variable akumulasi
        if (akumulasi <= 10) {
            hasil = akumulasi;
        } 
        // Jika diatas 10, maka hasil akan diassign dengan hasil pertambahan tiap digit dari akumulasi
        else {
            hasil = akumulasi / 10 + akumulasi % 10;
        }
        // Mengembalikkan boolean true/false jika dibandingkan hasil apakah sama dengan digit 14 dari NPM
        if (hasil == Integer.parseInt(npmString.substring(13,14))) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan NPM: ");
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            else {
                // Memanggil method extract dengan parameter npm dan print hasil kembali
                System.out.println(extract(npm));
                // Mengassign exitFlag menjadi true
                exitFlag = true;
                break;
            }
        }
        input.close();
    }
}