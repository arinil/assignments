package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private int numOfMataKuliah;
    private String[] masalahIRS = new String[19];
    private int numOfMasalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    public Mahasiswa(String nama, long npm){
        /* TODO: implementasikan kode Anda di sini */
        // Membuat constuctor untuk Mahasiswa
        this.nama = nama;
        this.npm = npm;
    }

    // Membuat getter
    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }

    public String[] getMasalahIRS() {
        cekIRS();
        return this.masalahIRS;
    }

    public int getTotalSKS() {
        return this.totalSKS;
    }

    public String getNama() {
        return this.nama;
    }

    public String getJurusan() {
        // Mengubah npm kedalam string lalu diambil kode jurusannya menggunakan substring dan dijadikan integer
        String npm = String.valueOf(this.npm);
        int kodeJurusanNpm = Integer.parseInt(npm.substring(2,4));
        // Mengassign jurusan sesuai kode
        if (kodeJurusanNpm == 01) {
            this.jurusan = "Ilmu Komputer";
        } 
        else {
            this.jurusan = "Sistem Informasi";
        }
        // Mengembalikkan jurusan
        return this.jurusan;
    }

    public String getKodeJurusan() {
        // Mengembalikkan IK jika jurusan ilmu komputer
        if (getJurusan().equals("Ilmu Komputer")) {
            return "IK";
        // Mengembalikkan SI jika jurusan sistem informasi
        } else {
            return "SI";
        }
    }

    public int getNumOfMataKuliah() {
        return this.numOfMataKuliah;
    }

    public long getNPM() {
        return this.npm;
    }

    public int getNumOfMasalahIRS() {
        return this.numOfMasalahIRS;
    }

    public int searchMataKuliah(MataKuliah mataKuliah) {
        for (int i = 0; i < this.getNumOfMataKuliah(); i++ ) {
            // Mencari nama mata kuliah pada array mataKuliah jika ditemukan akan dikembalikkan posisi mata kuliah tersebut
            if (this.mataKuliah[i] == mataKuliah) {
                return i;
            }
        }
        // Jika tidak ditemukan dikembalikkan -1
        return -1;
    }

    public void addMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable terdapatMataKuliah yang diassign dengan posisi mata kuliah dala array
        int terdapatMataKuliah = this.searchMataKuliah(mataKuliah);

        // Jika posisi -1 maka tidak terdapat mata kuliah dalam array (mata kuliah belum diambil sebelumnya)
        if (terdapatMataKuliah >= 0) {
            System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya.");
        } 
    
        // Jika jumlah daftar mahasiswa lebih atau sama dengan kapasitas, maka print kapasitas penuh
        else if (mataKuliah.getNumOfDaftarMahasiswa() >= mataKuliah.getKapasitas()) {
            System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya.");
        }

        // Jika mata kuliah lebih dari sama denga 10 maka print maksimal mata kuliah diambil hanya 10
        else if (this.numOfMataKuliah >= 10) {
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
        }

        // Jika condition-condition diatas tidak terpenuhi, maka mata kuliah akan di add serta jumlah sks bertambah
        else {
            this.mataKuliah[numOfMataKuliah] = mataKuliah;
            this.numOfMataKuliah++;
            this.totalSKS += mataKuliah.getSks();
            mataKuliah.addMahasiswa(this);
        }
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable dropPosition yang diassign dengan posisi mata kuliah pada array
        int dropPosition = this.searchMataKuliah(mataKuliah);

        // Jika posisi -1 maka tidak terdapat mata kuliah dalam array (mata kuliah belum diambil sebelumnya)
        if (dropPosition < 0) {
            System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil.");

        // Jika condition diatas tidak terpenuhi maka mata kuliah akan di drop serta sks akan dikurangi
        } else {
            this.mataKuliah[dropPosition] = null;
            this.totalSKS -= mataKuliah.getSks();
            this.numOfMataKuliah--;
            mataKuliah.dropMahasiswa(this);

            // Menggeser kekiri jika terdapat array kosong (null) diantara array yang terisi
            int i = dropPosition;
            while (i < this.numOfMataKuliah) {
                this.mataKuliah[i] = this.mataKuliah[i+1];
                i++;
            }
        }
    }

    public void cekIRS(){
        /* TODO: implementasikan kode Anda di sini */
        masalahIRS = new String[19];
        numOfMasalahIRS = 0;

        // Jika sks lebih dari 24 maka array masalahIRS akan diisi dengan string "SKS yang Anda ambil lebih dari 24"
        if (this.totalSKS > 24) {
            this.masalahIRS[numOfMasalahIRS] = "SKS yang Anda ambil lebih dari 24";
            // numOfMasalahIRS (jumlah masalah irs) akan bertambah 1
            this.numOfMasalahIRS++;
        }

        for (int i = 0; i < this.numOfMataKuliah; i++) {
            // Jika mata kuliah index i tidak sama dengan null dan kode mata kuliah yang diambil tidak sesuai dengan jurusan serta kode 
            // jurusan bukan "CS" maka array masalahIRS akan diisi dengan "Mata Kuliah (nama mata kuliah) tidak dapat diambil jurusan (IK/SI)"
            if (mataKuliah[i] != null && !mataKuliah[i].getKode().equals(this.getKodeJurusan()) && !mataKuliah[i].getKode().equals("CS")) {
                masalahIRS[numOfMasalahIRS] = "Mata Kuliah " + mataKuliah[i].getNama() + " tidak dapat diambil jurusan " + this.getKodeJurusan();
                numOfMasalahIRS++;
                break;
            }
        }
    }
  
    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengembalikkan nama mahasiswa
        return this.nama;
    }
}
