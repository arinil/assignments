package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int numOfDaftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        /* TODO: implementasikan kode Anda di sini */
        // Membuat constructor untuk mata kuliah
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    // Membuat getter
    public String getKode() {
        return this.kode;
    }

    public String getNama() {
        return this.nama;
    }

    public int getSks() {
        return this.sks;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public int getNumOfDaftarMahasiswa() {
        return this.numOfDaftarMahasiswa;
    }

    public void getDaftarMhs() {
        for (int i = 1; i < this.numOfDaftarMahasiswa; i++) {
            // Jika daftar mahasiswa index i-1 tidak sama dengan null maka akan diprint daftar mahasiswa
            if (daftarMahasiswa[i-1] != null) {
                System.out.println(i + ". " + daftarMahasiswa[i-1]);
            }
        }
    }

    public int searchMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.getNumOfDaftarMahasiswa(); i++ ) {
            // Mencari nama mahasiswa pada array daftarMahasiswa jika ditemukan akan dikembalikkan posisi mahasiswa tersebut
            if (this.daftarMahasiswa[i] == mahasiswa) {
                return i;
            }
        }
        // Jika tidak ditemukan dikembalikkan -1
        return -1;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        // Menambahkan mahasiswa ke array daftarMahasiswa serta menambahkan 1 numOfDaftarMahasiswa
        daftarMahasiswa[numOfDaftarMahasiswa] = mahasiswa;
        this.numOfDaftarMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable dropPosition yang diassign dengan posisi mata kuliah pada array
        int dropPosition = this.searchMahasiswa(mahasiswa);

        // Jika posisi lebih dari 0(terdapat mahasiswa) maka mahasiswa akan di drop
        if (dropPosition >= 0) {
            this.daftarMahasiswa[dropPosition] = null;
            this.numOfDaftarMahasiswa--;
        }

        // Menggeser kekiri jika terdapat array kosong (null) diantara array yang terisi
        int i = dropPosition;
        while (i < this.numOfDaftarMahasiswa) {
            this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
            i++;
        }
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengembalikkan nama mata kuliah
        return this.nama;
    }
}
