/*
Nama    : Arinil Haq
NPM     : 2006597430
Tanggal : 29 Maret 2021
Desc    : Program akan melakukan add/drop mata kuliah serta mencetak hasil ringkasan mahasiswa dan mata kuliah sesuai
          dengan input dari user.
*/
package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private int numOfDaftarMahasiswa;
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private int numOfDaftarMataKuliah;
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {
        /* TODO: Implementasikan kode Anda di sini */
        // Mengembalikkan mahasiswa sesuai dengan npm mahasiswa
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i].getNPM() == npm) {
                return daftarMahasiswa[i];
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {
        /* TODO: Implementasikan kode Anda di sini */
        // Mengembalikkan mata kuliah sesuai dengan nama mata kuliah
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].getNama().equals(namaMataKuliah)) {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila banyaknya matkul yang diambil mahasiswa sudah 9*/
        // Membuat variable mahasiswa yang diassign dengan method getMahasiswa dengan parameter input npm dari user
        Mahasiswa mahasiswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            /* TODO: Implementasikan kode Anda di sini */
            // Mengassign variable mataKuliah dengan memanggil method getMataKuliah dengan parameter nama mata kuliah inputan user
            MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
            // Memanggil method addMatkul pada class Mahasiswa
            mahasiswa.addMatkul(mataKuliah);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

       /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
        // Mengassign variable mahasiswa dengan memanggil method getMahasiswa dengan parameter npm inputan user
        Mahasiswa mahasiswa = getMahasiswa(npm);
        // Membuat variable boolean tanda
        boolean tanda = false;

        // Jika jumlah mata kuliah yang diambil adalah 0 (belum mengambil mata kuliah) maka tanda akan diassign
        // dengan true dan print "[DITOLAK] Belum ada mata kuliah yang diambil."
        if (mahasiswa.getNumOfMataKuliah() == 0) {
            tanda = true;
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }

        // Jika tanda false (sudah mengambil mata kuliah) maka mata kuliah akan di drop dengan memanggil mathod
        // dropMatkul pada class Mahasiswa
        if (!tanda) {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i = 0; i < banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                /* TODO: Implementasikan kode Anda di sini */
                MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
                mahasiswa.dropMatkul(mataKuliah);
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        // TODO: Isi sesuai format keluaran 
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        // Memanggil method getMahasiswa dengan parameter npm untuk mendapatkan nama mahasiswa
        System.out.println("Nama: " + getMahasiswa(npm));
        System.out.println("NPM: " + npm);
        // Memanggil getJurusan pada kelas Mahasiswa
        System.out.println("Jurusan: " + getMahasiswa(npm).getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        // Membuat variable getMatkul yang diassign dengan mata kuliah yang diambil oleh mahasiswa
        MataKuliah[] getMatkul = getMahasiswa(npm).getMataKuliah();
        boolean tandaMatkul = false;

        // Jika getMatkul index 0 adalah null (tidak ada mata kuliah yang diambil) maka tandaMatkul akan
        // diassign dengan true, dan dicetak "Belum ada mata kuliah yang diambil"
        if (getMatkul[0] == null) {
            tandaMatkul = true;
            System.out.println("Belum ada mata kuliah yang diambil");
        }

        // Jika tandaMatkul false (ada mata kuliah yang diambil) maka akan dicetak daftar mata kuliah
        if (!tandaMatkul) {
            for (int i = 0; i < getMahasiswa(npm).getNumOfMataKuliah(); i++) {
                if (getMatkul[i] != null) {
                    System.out.println((i+1) + ". " + getMatkul[i].getNama());
                }
            }
        }

        System.out.println("Total SKS: " + getMahasiswa(npm).getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");
        /* TODO: Cetak hasil cek IRS
        Handle kasus jika IRS tidak bermasalah */
        boolean tandaIRS = false;

        // Jika array masalahIRS index 0 sama dengan null (IRS tidak bermasalah) maka boolean tanda IRS akan
        // diassign dengan true, dan akan dicetak "IRS tidak bermasalah" 
        if (getMahasiswa(npm).getMasalahIRS()[0] == null) {
            tandaIRS = true;
            System.out.println("IRS tidak bermasalah.");
        }

        // Jika tandaIRS false (IRS bermasalah) maka akan dicetak hasil cek IRS
        if (!tandaIRS) {
            for (int i = 0; i < getMahasiswa(npm).getNumOfMasalahIRS(); i++) {
                if (getMahasiswa(npm).getMasalahIRS()[i] != null) {
                    System.out.println((i+1) + ". " + getMahasiswa(npm).getMasalahIRS()[i]);
                }
            }
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        
        // TODO: Isi sesuai format keluaran
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        // Memanggil method getNama pada kelas MataKuliah untuk mendapatkan nama mata kuliah
        System.out.println("Nama mata kuliah: " + getMataKuliah(namaMataKuliah).getNama());
        // Memanggil method getKode pada kelas MataKuliah untuk mendapatkan kode mata kuliah
        System.out.println("Kode: " + getMataKuliah(namaMataKuliah).getKode());
        // Memanggil method getSks pada kelas MataKuliah untuk mendapatkan sks mata kuliah
        System.out.println("SKS: " + getMataKuliah(namaMataKuliah).getSks());
        // Memanggil method getNumOfDaftarMahasiswa pada kelas MataKuliah untuk mendapatkan jumlah mahasiswa yang mendaftar
        System.out.println("Jumlah mahasiswa: " + getMataKuliah(namaMataKuliah).getNumOfDaftarMahasiswa());
        // Memanggil method getKapasitas pada kelas MataKuliah untuk mendapatkan kapasitas mata kuliah
        System.out.println("Kapasitas: " + getMataKuliah(namaMataKuliah).getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */
        boolean tandaMhs = false;

        // Jika daftar mahasiswa pada mata kuliah adalah 0 (tidak ada mahasiswa yang mengambil) maka tandaMhs akan
        // diassign dengan true, dan akan dicetak "Belum ada mahasiswa yang mengambil mata kuliah ini."
        if (getMataKuliah(namaMataKuliah).getNumOfDaftarMahasiswa() == 0) {
            tandaMhs = true;
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }

        // Jika tandaMhs false (ada mahasiswa yang mengambil) maka akan dicetak nama mahasiswa yang mengambil mata kuliah
        if (!tandaMhs) {
            for (int i = 0; i < getMataKuliah(namaMataKuliah).getNumOfDaftarMahasiswa(); i++) {
                if (getMataKuliah(namaMataKuliah).getDaftarMahasiswa()[i] != null) {
                    System.out.println((i+1) + ". " + getMataKuliah(namaMataKuliah).getDaftarMahasiswa()[i].getNama());
                }
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }
    }

    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            // Membuat variable kode dan nama yang diassign dengan hasil split input user
            String kode = dataMatkul[0];
            String nama = dataMatkul[1];
            // Membuat instance mata kuliah
            MataKuliah mataKuliah = new MataKuliah(kode, nama, sks, kapasitas);
            // Menggasign hasil input user yang telah masuk ke dalam instance mataKuliah kedalam array daftarMataKuliah
            daftarMataKuliah[numOfDaftarMataKuliah] = mataKuliah;
            // numOfDaftarMataKuliah bertambah 1
            numOfDaftarMataKuliah++;
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* TODO: Buat instance mata kuliah dan masukkan ke dalam Array */
            // Membuat variable nama yang diassign dengan hasil split input user
            String nama = dataMahasiswa[0];
            // Membuat instance variable mahasiswa  
            Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
            // Mengassign hasil input user yang telah masuk ke dalam instance mahasiswa ke dalam array daftarMahasiswa
            daftarMahasiswa[numOfDaftarMahasiswa] = mahasiswa;
            // numOfDaftarMahasiswa bertambah 1
            numOfDaftarMahasiswa++;
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }
}
