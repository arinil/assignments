package assignments.assignment3;

public class Dosen extends ElemenFasilkom {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah mataKuliah;

    // Constructor untuk Dosen
    public Dosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("Dosen", nama);
    }

    // Getter mataKuliah
    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */ 
        // Jika mataKuliah tidak null akan dicetak ditolak sesuai format
        if (this.mataKuliah != null) {
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", toString(), this.mataKuliah.toString()));
            return;
        }
        // Jika dosen dari mata kuliah tersebut tidak null maka akan dicetak ditolak sesuai format
        else if (mataKuliah.getDosen() != null) {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah.getNama()));
            return;
        }
        // Jika condition-condition diatas tidak terpenuhi mata kuliah akan di add dosen
        else {
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.println(String.format("%s mengajar mata kuliah %s", toString(), mataKuliah.getNama()));
        }
    }

    public void dropMataKuliah() {
        /* TODO: implementasikan kode Anda di sini */
        // Jika mata kuliah milik dosen tidak null (dosen telah mengajar mata kuliah) maka mata kuliah akan di drop dosen
        if (mataKuliah != null) {
            mataKuliah.dropDosen();
            System.out.println(String.format("%s berhenti mengajar %s", toString(), mataKuliah.toString()));
            this.mataKuliah = null;
        }
        // Jika mata kuliah null (dosen sedang tidak mengajar mata kuliah) maka akan dicetak ditolak sesuai format
        else {
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", toString()));
        }
    }
}