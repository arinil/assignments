package assignments.assignment3;

import java.util.Arrays;

public abstract class ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    private int numOfTelahMenyapa;

    // Constructor untuk ElemenFasilkom
    public ElemenFasilkom(String tipe, String nama) {
        this.tipe = tipe;
        this.nama = nama;
        this.friendship = 0;
    }

    // Getter
    public String getTipe() {
        return this.tipe;
    }

    public String getNama() {
        return this.nama;
    }

    public int getFriendship() {
        return this.friendship;
    }
    
    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    public int getNumOfTelahMenyapa() {
        return this.numOfTelahMenyapa;
    }

    // Setter untuk setFriendship
    public void setFriendship(int angka) {
        this.friendship += angka;
        // Jika friendship bernilai minus maka friendship akan diassign dengan 0
        if (this.friendship < 0) {
            this.friendship = 0;
        }
        // Jika friendship bernilai lebih dari 100 maka friendship akan diassign dengan 100
        else if (this.friendship > 100) {
            this.friendship = 100;
        }
    }

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        /* TODO: implementasikan kode Anda di sini */
        // boolean cek = false;
        for (int i = 0; i < telahMenyapa.length; i++) {
            if (telahMenyapa[i] == elemenFasilkom) {
                System.out.println("[DITOLAK] " + toString() + " telah menyapa " + elemenFasilkom.toString() + " hari ini");
                return;
            }            
        }
        // Menambahkan elemenFasilkom ke array telahMenyapa dan mencetak sesuai format
        telahMenyapa[numOfTelahMenyapa] = elemenFasilkom;
        numOfTelahMenyapa++;
        System.out.println(this.nama + " menyapa dengan " + elemenFasilkom.getNama());

        // Menambahkan this kedalam array telahMenyapa milik elemenFasilkom
        elemenFasilkom.telahMenyapa[numOfTelahMenyapa] = this;
        elemenFasilkom.numOfTelahMenyapa++;
    }

    public void resetMenyapa() {
        /* TODO: implementasikan kode Anda di sini */
        // Array telahMenyapa akan direset
        Arrays.fill(telahMenyapa, null);
        numOfTelahMenyapa = 0;
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable elemenKantin yang diassign dengan pejual
        ElemenKantin elemenKantin = (ElemenKantin) penjual;

        // Mencari namaMakanan pada daftarMakanan yang dijual oleh penjual
        for (int i = 0; i < elemenKantin.getNumOfDaftarMakanan(); i++ ) {
            // Jika ditemukan akan menambahkan friendship pembeli dan penjual dengan 1 dan mencetak berhasil membeli sesuai format
            if (elemenKantin.getDaftarMakanan()[i].getNama().equals(namaMakanan)) {
                pembeli.setFriendship(1);
                penjual.setFriendship(1);
                System.out.println(pembeli.getNama() + " berhasil membeli " + namaMakanan + " seharga " + elemenKantin.getDaftarMakanan()[i].getHarga());
                return;
            }
        }
        // Jika tidak ditemukan akan dicetak ditolak sesuai format
        System.out.println("[DITOLAK] " + penjual.getNama() + " tidak menjual " + namaMakanan);
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengembalikkan nama dari elemenFasilkom
        return this.nama;
    }
}