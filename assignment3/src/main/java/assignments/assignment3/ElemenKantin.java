package assignments.assignment3;

public class ElemenKantin extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private Makanan[] daftarMakanan = new Makanan[10];

    private int numOfDaftarMakanan;

    // Constructor untuk ElemenKantin
    public ElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        super("ElemenKantin", nama);
    }

    // Getter
    public Makanan[] getDaftarMakanan() {
        return this.daftarMakanan;
    }

    public int getNumOfDaftarMakanan() {
        return this.numOfDaftarMakanan;
    }

    public void setMakanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        // For loop untuk mencari apakah makanan yang akan di set sudah ada dalam daftar makanan
        for (int i = 0; i < numOfDaftarMakanan; i++) {
            // Jika sudah ada maka akan dicetak ditolak *nama makanan* sudah terdaftar
            if (daftarMakanan[i].getNama().equals(nama)) {
                System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
                return;
            }
        }
        // Jika belum terdaftar maka akan dibuat objek makanan baru yang diisi dengan parameter nama dan harga
        Makanan makanan = new Makanan(nama, harga);
        // Menambahkan makanan kedalam array daftarMakanan dan dicetak berhasil mendaftarkan sesuai format
        daftarMakanan[numOfDaftarMakanan] = makanan;
        numOfDaftarMakanan++;
        System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", toString(), nama, harga));
    }
}