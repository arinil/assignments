package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private int numOfMataKuliah;
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    // Constructor untuk Mahasiswa
    public Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        super("Mahasiswa", nama);
        this.npm = npm;
    }

    // Getter
    public MataKuliah[] getMataKuliah() {
        return this.daftarMataKuliah;
    }

    public int getNumOfMataKuliah() {
        return this.numOfMataKuliah;
    }

    public long getNpm() {
        return this.npm;
    }

    public String getTanggalLahir() {
        return this.tanggalLahir;
    }
    
    public String getJurusan() {
        return this.jurusan;
    }

    // Method searchMataKuliah untuk mencari mataKuliah dalam array daftarMataKuliah
    public int searchMataKuliah(MataKuliah mataKuliah) {
        for (int i = 0; i < numOfMataKuliah; i++) {
            // Mencari nama mata kuliah pada array mataKuliah jika ditemukan akan dikembalikkan posisi mata kuliah tersebut
            if (daftarMataKuliah[i] == mataKuliah) {
                return i;
            }
        }
        // Jika tidak ditemukan dikembalikkan -1
        return -1;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // For loop untuk mencari apakah mataKuliah sudah ada dalam array daftarMataKuliah
        for (int i = 0; i < numOfMataKuliah; i++) {
            // Jika sudah ada akan dicetak ditolak *nama mata kuliah* telah diambil sebelumnya
            if (this.daftarMataKuliah[i] == mataKuliah) {
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya");
                return;
            }
        }
        // Jika jumlah mahasiswa melebihi kapasitas akan dicetak ditolak sesuai format
        if (mataKuliah.getNumOfDaftarMahasiswa() >= mataKuliah.getKapasitas()) {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya");
        }

        // Jika condition-condition diatas tidak terpenuhi, maka mata kuliah akan di add dan akan dicetak berhasil menambahkan sesuai format
        else {
            this.daftarMataKuliah[numOfMataKuliah] = mataKuliah;
            this.numOfMataKuliah++;
            mataKuliah.addMahasiswa(this);
            System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", toString(), mataKuliah.getNama()));
        }
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // For loop untuk mencari apakah mataKuliah sudah ada dalam array daftarMataKuliah
        for (int i = 0; i < numOfMataKuliah; i++) {
            // Jika sudah ada akan dicetak ditolak *nama mata kuliah* belum pernah diambil
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] == mataKuliah) {
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " belum pernah diambil");
                return;
            }
        }
        
        // Membuat variable dropPosition berupa posisi mataKuliah dalam array daftarMataKuliah
        int dropPosition = searchMataKuliah(mataKuliah);
        // Jika dropPosition buka -1 (terdapat mata kuliah tersebut) akan di drop
        if (dropPosition != -1) {
            this.daftarMataKuliah[dropPosition] = null;
            this.numOfMataKuliah--;
            mataKuliah.dropMahasiswa(this);

            // Menggeser kekiri jika terdapat array kosong (null) diantara array yang terisi
            while (dropPosition < this.numOfMataKuliah) {
            this.daftarMataKuliah[dropPosition] = this.daftarMataKuliah[dropPosition+1];
            dropPosition++;
            }

            // Mencetak berhasil drop mata kuliah sesuai format
            System.out.println(toString() + " berhasil drop mata kuliah " + mataKuliah.getNama());
        }
    }

    public String extractTanggalLahir(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable npmString berupa parameter npm dalam string
        String npmString = String.valueOf(this.npm);

        // Membuat string tanggal menggunakan substring dari npmString
        String tanggal = npmString.substring(4,6);
        if (npmString.substring(4, 5).equals("0")) {
            tanggal = npmString.substring(5, 6);
        }

        // Membuat string bulan menggunakan substring dari npmString
        String bulan = npmString.substring(6,8);
        if (npmString.substring(6, 7).equals("0")) {
            bulan = npmString.substring(7, 8);
        }

        // Membuat string tahun menggunakan substring dari npmString
        String tahun = npmString.substring(8,12);

        // Menset dan mengembalikkan tanggalLahir 
        this.tanggalLahir = tanggal + "-" + bulan + "-" + tahun;
        return this.tanggalLahir;
    }

    public String extractJurusan(long npm) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable npmString yang diassign dengan npm yang telah diubah menjadi string
        String npmString = String.valueOf(this.npm);
        // Membuat variable kodeJurusanNpm yang diassign dengan substring(2,4) dari npmString
        int kodeJurusanNpm = Integer.parseInt(npmString.substring(2,4));
        // Mengassign jurusan sesuai kode
        if (kodeJurusanNpm == 01) {
            this.jurusan = "Ilmu Komputer";
        } 
        else {
            this.jurusan = "Sistem Informasi";
        }
        // Mengembalikkan jurusan
        return this.jurusan;
    }

    // Method cekMataKuliahSama untuk mengecek apakah objek mahasiswa menyapa objek dosen dimana mereka 
    // terhubung dalam 1 mata kuliah yang sama
    public void cekMataKuliahSama() {
        ElemenFasilkom[] telahMenyapa = getTelahMenyapa();
        for (int i = 0; i < getNumOfTelahMenyapa(); i++) {
            // Jika tipe dari elemen fasilkom yang telah disapa adalah dosen akan dibuat variable dosen
            if (telahMenyapa[i] != null && telahMenyapa[i].getTipe().equals("Dosen")) {
                Dosen dosen = (Dosen) telahMenyapa[i];
                for (int j = 0; j < numOfMataKuliah; j++) {
                    // Jika terhubung pada 1 mata kuliah sama maka friendship dari mahasiswa dan dosen akan bertambah 2
                    if (daftarMataKuliah[j] != null && daftarMataKuliah[j] == dosen.getMataKuliah()) {
                        setFriendship(2);
                        telahMenyapa[i].setFriendship(2);
                        break;
                    }
                }
            }
        }
    }
}