package assignments.assignment3;

import java.util.Scanner;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0; 

    // Getter
    public static ElemenFasilkom[] getDaftarElemenFasilkom() {
        return daftarElemenFasilkom;
    }

    public static MataKuliah[] getDaftarMataKuliah() {
        return daftarMataKuliah;
    }

    public static int getTotalMataKuliah() {
        return totalMataKuliah;
    }

    public static int getTotalElemenFasilkom() {
        return totalElemenFasilkom;
    }

    private static void addMahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat objek Mahasiswa baru dengan parameter nama dan npm
        Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
        // Menambahkan objek mahasiswa ke dalam daftarElemenFasilkom dan mencetak sesuai format
        daftarElemenFasilkom[totalElemenFasilkom] = mahasiswa;
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    private static void addDosen(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat objek Dosen baru dengan parameter nama
        Dosen dosen = new Dosen(nama);
        // Menambahkan objek dosen ke dalam daftarElemenFasilkom dan mencetak sesuai format
        daftarElemenFasilkom[totalElemenFasilkom] = dosen;
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    private static void addElemenKantin(String nama) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat objek ElemenKantin dengan parameter nama 
        ElemenKantin elemenKantin = new ElemenKantin(nama);
        // Menambahkan objek tersebut ke dalam daftarElemenFasilkom dan mencetak sesuai format
        daftarElemenFasilkom[totalElemenFasilkom] = elemenKantin;
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    private static void menyapa(String objek1, String objek2) {
        /* TODO: implementasikan kode Anda di sini */
        // Jika objek1 dan objek2 tidak sama, akan membuat interaksi menyapa
        if (!objek1.equals(objek2)) {
            // Membuat variable elemenFasilkom1 dan elemenFasilkom2 dengan memanggil method getElemenFasilkom
            ElemenFasilkom elemenFasilkom1 = getElemenFasilkom(objek1);
            ElemenFasilkom elemenFasilkom2 = getElemenFasilkom(objek2);

            elemenFasilkom1.menyapa(elemenFasilkom2);
        }
        // Jika objek1 dan objek2 tidak sama, akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        }
    }

    private static void addMakanan(String objek, String namaMakanan, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable tipeObjek untuk mengetahui tipe dari parameter objek
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipeObjek adalah ElemenKantin maka akan menambahkan makanan dengan parameter namaMakanan dan harga ke dalam objek
        if (tipeObjek.equals("ElemenKantin")) {
            ElemenKantin elemenKantin = (ElemenKantin) getElemenFasilkom(objek);
            elemenKantin.setMakanan(namaMakanan, harga);
        }
        // Jika tipeObjek bukan ElemenKantin maka akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
        }
    }

    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        /* TODO: implementasikan kode Anda di sini */
        //// Membuat variable tipeObjek2 untuk mengetahui tipe dari parameter objek2
        String tipeObjek2 = getElemenFasilkom(objek2).getTipe();

        // Jika tipeObjek2 bukan ElemenKantin akan dicetak ditolak sesuai format
        if (!tipeObjek2.equals("ElemenKantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
        // Jika tipeObjek2 merupakan ElemenKantin namun objek1 dan objek2 merupakan objek yang sama
        // akan dicetak ditolak sesuai format
        else if (tipeObjek2.equals("ElemenKantin") && objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        }
        // Jika condition-condition diatas tidak terpenuhi maka pembeli akan membeli makanan penjual
        else {
            ElemenFasilkom pembeli = getElemenFasilkom(objek1);
            ElemenFasilkom penjual = getElemenFasilkom(objek2);

            pembeli.membeliMakanan(pembeli, penjual, namaMakanan);
        }
    }

    private static void createMatkul(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat objek MataKuliah dengan parameter nama dan kapasitas dan menyimpannya ke list daftarMataKuliah
        MataKuliah mataKuliah = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah] = mataKuliah;
        totalMataKuliah++;
        // Mencetak sesuai format
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    private static MataKuliah getMataKuliah(String namaMataKuliah) {
        // Mengembalikkan mata kuliah sesuai dengan nama mata kuliah
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].getNama().equals(namaMataKuliah)) {
                return daftarMataKuliah[i];
            }
        }
        // Jika tidak ditemukan dikembalikan null
        return null;
    }

    private static ElemenFasilkom getElemenFasilkom(String objek) {
        for (int i = 0; i < totalElemenFasilkom; i++) {
            // Jika nama dari elemenFasilkom sama dengan parameter objek akan dikembalikan objek elemenFasilkom tersebut
            if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[i].getNama().equals(objek)) {
                return daftarElemenFasilkom[i];
            }
        }
        // Jika tidak ditemukan dikembalikan null
        return null;
    }

    private static void addMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable tipeObjek yang diassign dengan tipe dari parameter objek
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipe objek adalah Mahasiswa, maka mahasiswa akan add matkul
        if (tipeObjek.equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa) getElemenFasilkom(objek);
            MataKuliah getNamaMatkul = getMataKuliah(namaMataKuliah);
            mahasiswa.addMatkul(getNamaMatkul);
        }
        // Jika tipe objek bukan Mahasiswa, akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }

    }

    private static void dropMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable tipeObjek yang diassign dengan tipe dari parameter objek
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipe objek adalah Mahasiswa, maka mahasiswa akan drop matkul
        if (tipeObjek.equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa) getElemenFasilkom(objek);
            MataKuliah getNamaMatkul = getMataKuliah(namaMataKuliah);
            mahasiswa.dropMatkul(getNamaMatkul);
        }
        // Jika tipe objek bukan Mahasiswa, akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable tipeObjek yang diassign dengan tipe dari parameter objek
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipe objek adalah Dosen, maka dosen akan mengajar matkul
        if (tipeObjek.equals("Dosen")) {
            Dosen dosen = (Dosen) getElemenFasilkom(objek);
            MataKuliah getNamaMatkul = getMataKuliah(namaMataKuliah);
            dosen.mengajarMataKuliah(getNamaMatkul);
        }
        // Jika tipe objek bukan Dosen, akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    private static void berhentiMengajar(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable tipeObjek yang diassign dengan tipe dari parameter objek
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipe objek adalah dosen, maka dosen akan berhenti mengajar 
        if (tipeObjek.equals("Dosen")) {
            Dosen dosen = (Dosen) getElemenFasilkom(objek);
            dosen.dropMataKuliah();
        }
        // Jika tipe objek bukan dosen, akan dicetak ditolak sesuai format
        else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    private static void ringkasanMahasiswa(String objek) {
        /* TODO: implementasikan kode Anda di sini */
        // Memanggil method getMahasiswa dengan parameter objek untuk mendapatkan nama mahasiswa
        String tipeObjek = getElemenFasilkom(objek).getTipe();

        // Jika tipeObjek bukan merupakan Mahasiswa akan dicetak ditolak sesuai format
        if (!tipeObjek.equals("Mahasiswa") ) {
            System.out.println("[DITOLAK] " + objek+ " bukan merupakan seorang mahasiswa");
        }
        // Jika tipeObjek merupakan Mahasiswa akan dicetak ringkasan dari mahasiwa tersebut
        else {
            Mahasiswa mahasiswa = (Mahasiswa) getElemenFasilkom(objek);
            MataKuliah[] getMatkul = mahasiswa.getMataKuliah();
            long npm = mahasiswa.getNpm();

            // Memanggil method getNama pada kelas Mahasiswa untuk mendapatkan nama mahasiswa
            System.out.println("Nama: " + mahasiswa.getNama());
            // Memanggil method extractTanggalLahir pada kelas Mahasiswa untuk mendapatkan tanggal lahir mahasiswa tsb
            System.out.println("Tanggal lahir: " + mahasiswa.extractTanggalLahir(npm));
            // Memanggil method extractJurusan pada kelas Mahasiswa untuk mendapatkan jurusan dari mahasiswa
            System.out.println("Jurusan: " + mahasiswa.extractJurusan(npm));
            System.out.println("Daftar Mata Kuliah:");
            // Jika getMatkul index 0 adalah null (belum ada mata kuliah diambil) akan dicetak sesuai format
            if (getMatkul[0] == null) {
                System.out.println("Belum ada mata kuliah yang diambil");
            }
            // Jika getMatkul index 0 bukan null (ada mata kuliah diambil) maka akan dicetak nama-nama mata kuliah yang diambil
            else {
                for (int i = 0; i < mahasiswa.getNumOfMataKuliah(); i++) {
                    if (getMatkul[i] != null) {
                        System.out.println((i+1) + ". " + getMatkul[i].getNama());
                    }
                }
            }
        }
    }

    private static void ringkasanMataKuliah(String namaMataKuliah) {
        /* TODO: implementasikan kode Anda di sini */
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);
        // Memanggil method getNama pada kelas MataKuliah untuk mendapatkan nama mata kuliah
        System.out.println("Nama mata kuliah: " + mataKuliah.getNama());
        // Memanggil method getNumOfDaftarMahasiswa pada kelas MataKuliah untuk mendapatkan jumlah mahasiswa yang mendaftar
        System.out.println("Jumlah mahasiswa: " + mataKuliah.getNumOfDaftarMahasiswa());
        // Memanggil method getKapasitas pada kelas MataKuliah untuk mendapatkan kapasitas mata kuliah
        System.out.println("Kapasitas: " + mataKuliah.getKapasitas());

        // Mencetak dosen pengajar
        if (mataKuliah.getDosen() == null) {
            System.out.println("Dosen pengajar: Belum ada");
        } else {
            System.out.println("Dosen pengajar: " + mataKuliah.getDosen().getNama());
        }

        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        // Jika daftar mahasiswa pada mata kuliah adalah 0 (tidak ada mahasiswa yang mengambil) akan dicetak "Belum ada mahasiswa yang mengambil mata kuliah ini."
        if (getMataKuliah(namaMataKuliah).getNumOfDaftarMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        }

        // Jika tidak sama dengan 0 (ada mahasiswa yang mengambil) maka akan dicetak nama mahasiswa yang mengambil mata kuliah
        else {
            for (int i = 0; i < getMataKuliah(namaMataKuliah).getNumOfDaftarMahasiswa(); i++) {
                if (getMataKuliah(namaMataKuliah).getDaftarMahasiswa()[i] != null) {
                    System.out.println((i+1) + ". " + getMataKuliah(namaMataKuliah).getDaftarMahasiswa()[i].getNama());
                }
            }
        }
    }

    private static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < totalElemenFasilkom; i++) {
            // Jika elemenFasilkom tidak null
            if (daftarElemenFasilkom[i] != null) {
                // Jika tipe dari elemen fasilkom adalah Mahasiswa, maka akan memanggil method cekMataKuliahSama
                if (daftarElemenFasilkom[i].getTipe().equals("Mahasiswa")) {
                    Mahasiswa mahasiswa = (Mahasiswa) daftarElemenFasilkom[i];
                    mahasiswa.cekMataKuliahSama();
                } 

                // Jika numOfTelahMenyapa lebih dari atau sama dengan 1/2 dari totalElemenFasilkom,
                // friendship akan ditambahkan dengan 10
                if (daftarElemenFasilkom[i].getNumOfTelahMenyapa() >= ((totalElemenFasilkom-1)/2)) {
                    daftarElemenFasilkom[i].setFriendship(10);
                }
                // Jika kurang dari 1/2 dari totalElemenFasilkom pada main class, friendship akan dikurangi dengan 5
                else {
                    daftarElemenFasilkom[i].setFriendship(-5);
                }
            }
            // Mereset array telahMenyapa dari elemen fasilkom
            daftarElemenFasilkom[i].resetMenyapa();
        }
        // Mencetak sesuai format dan memanggil method friendshipRanking
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    private static void friendshipRanking() {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable temp
        ElemenFasilkom temp;
        for (int i = 0; i < totalElemenFasilkom; i++) {     
            for (int j = i+1; j < totalElemenFasilkom; j++) {  
                // Mengurutkan array daftarElemenFasilkom berdasarkan nilai friendship
                if (daftarElemenFasilkom[i].getFriendship() < daftarElemenFasilkom[j].getFriendship()) {      //swap elements if not in order
                    temp = daftarElemenFasilkom[i];    
                    daftarElemenFasilkom[i] = daftarElemenFasilkom[j];    
                    daftarElemenFasilkom[j] = temp;    
                } 
                // Jika nilai friendship sama, akan diurutkan bersadarkan alphabet
                else if (daftarElemenFasilkom[i].getFriendship() == daftarElemenFasilkom[j].getFriendship()) { 
                    if (daftarElemenFasilkom[j].getNama().compareTo(daftarElemenFasilkom[i].getNama()) < 0) {
                        temp = daftarElemenFasilkom[i];
                        daftarElemenFasilkom[i] = daftarElemenFasilkom[j];
                        daftarElemenFasilkom[j] = temp;
                    }
                }
            }
        }

        // Mencetak elemen fasilkom dan nilai friendship masing-masing elemen
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i] != null) {
                System.out.println(String.format("%d. %s(%d)", i+1, daftarElemenFasilkom[i].getNama(), daftarElemenFasilkom[i].getFriendship()));
            }
        }

    }

    private static void programEnd() {
        /* TODO: implementasikan kode Anda di sini */
        // Mencetak sesuai format dan memanggil methos friendshipRanking
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
        
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}