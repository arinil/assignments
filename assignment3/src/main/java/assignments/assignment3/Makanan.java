package assignments.assignment3;

public class Makanan {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;

    private long harga;

    // Constructor untuk Makanan
    public Makanan(String nama, long harga) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.harga = harga;
    }

    // Getter untuk nama dan harga
    public String getNama() {
        return this.nama;
    }

    public long getHarga() {
        return this.harga;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengembalikkan nama makanan
        return this.nama;
    }
}