package assignments.assignment3;

public class MataKuliah {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;
    private int numOfDaftarMahasiswa;

    // Constructor untuk MataKuliah
    public MataKuliah(String nama, int kapasitas) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    // Getter
    public String getNama() {
        return this.nama;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public Dosen getDosen() {
        return this.dosen;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public int getNumOfDaftarMahasiswa() {
        return this.numOfDaftarMahasiswa;
    }

    public int searchMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < numOfDaftarMahasiswa; i++ ) {
            // Mencari nama mahasiswa pada array daftarMahasiswa jika ditemukan akan dikembalikkan posisi mahasiswa tersebut
            if (this.daftarMahasiswa[i] == mahasiswa) {
                return i;
            }
        }
        // Jika tidak ditemukan dikembalikkan -1
        return -1;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        // Menambahkan mahasiswa ke array daftarMahasiswa serta menambahkan 1 numOfDaftarMahasiswa
        daftarMahasiswa[numOfDaftarMahasiswa] = mahasiswa;
        this.numOfDaftarMahasiswa++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        // Membuat variable dropPosition yang diassign dengan posisi mahasiswa pada array daftarMahasiswa
        int dropPosition = this.searchMahasiswa(mahasiswa);

        // Jika posisi lebih dari 0 (terdapat mahasiswa) maka mahasiswa akan di drop
        if (dropPosition >= 0) {
            this.daftarMahasiswa[dropPosition] = null;
            this.numOfDaftarMahasiswa--;
        }

        // Menggeser kekiri jika terdapat array kosong (null) diantara array yang terisi
        int i = dropPosition;
        while (i < this.numOfDaftarMahasiswa) {
            this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
            i++;
        }
    }

    public void addDosen(Dosen dosen) {
        /* TODO: implementasikan kode Anda di sini */
        // Mengassign this.dosen dengan parameter dosen
        this.dosen = dosen;
    }

    public void dropDosen() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengassign this.dosen dengan null (mendrop dosen)
        this.dosen = null;
    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        // Mengembalikkan nama mata kuliah
        return this.nama;
    }
}