package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 500);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat label untuk Judul "Detail Ringkasan Mahasiswa" dan men-setting label
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat main panel bernama panelCenter
        JPanel panelCenter = new JPanel();
        // Membuat panel untuk button
        JPanel panelSelesai = new JPanel();
        // Membuat label dan button
        JLabel namaLabel = new JLabel();
        JLabel npmLabel = new JLabel();
        JLabel jurusanLabel = new JLabel();
        JLabel daftarMataKuliahLabel = new JLabel();
        JLabel totalSKSLabel = new JLabel();
        JLabel hasilPengecekanLabel = new JLabel();
        JButton selesaiButton = new JButton("Selesai");

        // Mencari banyak kolom untuk dimasukkan ke grid layout
        // Kolom yang tidak berubah jumlahnya adalah 8
        int kolom = 8;
        // Jika masalah irs dari mahasiswa tidak ada maka kolom akan bertambah 1 
        if (mahasiswa.getBanyakMasalahIRS() == 0){
            kolom += 1;
        }
        // Jika mahasiswa memiliki masalah irs maka kolom akan bertambah dengan banyak irs
        else {
            kolom += mahasiswa.getBanyakMasalahIRS();
        }
        
        // Jika mahasiswa tidak memiliki mata kuliah yang diambil maka kolom akan berambah 1
        if (mahasiswa.getBanyakMatkul() == 0) {
            kolom += 1;
        }
        // Jika mahasiswa memiliki mata kuliah yang diambil maka kolom akan bertambah dengan jumlah mata kuliah
        else {
            kolom += mahasiswa.getBanyakMatkul();
        }
        // Men-setting panelCenter dengan grid layout dengan kolom sesuai perhitungan diatas dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(kolom, 1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan setiap label
        namaLabel.setText(String.format("Nama: %s", mahasiswa.getNama()));
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);

        npmLabel.setText(String.format("NPM: %d", mahasiswa.getNpm()));
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);

        jurusanLabel.setText(String.format("Jurusan: %s", mahasiswa.getJurusan()));
        jurusanLabel.setHorizontalAlignment(JLabel.CENTER);  
        jurusanLabel.setFont(SistemAkademikGUI.fontGeneral);

        daftarMataKuliahLabel.setText("Daftar Mata Kuliah:");
        daftarMataKuliahLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMataKuliahLabel.setFont(SistemAkademikGUI.fontGeneral);

        totalSKSLabel.setText(String.format("Total SKS: %d", mahasiswa.getTotalSKS()));
        totalSKSLabel.setHorizontalAlignment(JLabel.CENTER);
        totalSKSLabel.setFont(SistemAkademikGUI.fontGeneral);

        hasilPengecekanLabel.setText("Hasil Pengecekan IRS:");
        hasilPengecekanLabel.setHorizontalAlignment(JLabel.CENTER);
        hasilPengecekanLabel.setFont(SistemAkademikGUI.fontGeneral);

        // // Men-setting tampilan selesaiButton
        selesaiButton.setFont(SistemAkademikGUI.fontGeneral);
        selesaiButton.setBackground(new Color(205, 227, 255));
        selesaiButton.setOpaque(true);
        // Menambahkan action listener untuk selesaiButton
        selesaiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });
        // Menambahkan button selesai ke panel selesai dan mengatur warna background
        panelSelesai.add(selesaiButton);
        panelSelesai.setBackground(HomeGUI.warnaKuning);

        // Menambahkan label ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(namaLabel);
        panelCenter.add(npmLabel);
        panelCenter.add(jurusanLabel);
        panelCenter.add(daftarMataKuliahLabel);

        // Menampilkan daftar mata kuliah
        // Jika mahasiswa tidak memiliki mata kuliah yang diambil
        if (mahasiswa.getBanyakMatkul() <= 0) {
            // Membuat label "Belum ada mata kuliah yang diambil." serta memasukkannya ke panelCenter
            JLabel daftarMatkulLabel = new JLabel("Belum ada mata kuliah yang diambil.");
            daftarMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
            daftarMatkulLabel.setFont(new Font("Century Gothic", Font.BOLD , 14));
            panelCenter.add(daftarMatkulLabel);
        }
        // Jika mahasiswa memiliki mata kuliah yang diambil
        else {
            // Membuat array listMatkul dan diisi dengan mata kuliah yang diambil oleh mahasiswa
            MataKuliah[] listMatkul = new MataKuliah[mahasiswa.getBanyakMatkul()];
            listMatkul = mahasiswa.getMataKuliah();
            // Membuat array daftarMatkulString dan daftarMatkul
            String[] daftarMatkulString = new String[mahasiswa.getBanyakMatkul()];
            JLabel[] daftarMatkul;
            // For loop untuk menambahkan array daftarMatkulString dengan nama mata kuliah sesuai format
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
                if (listMatkul[i] != null) {
                    daftarMatkulString[i] = String.format("%d. %s\n", i+1, listMatkul[i].getNama());
                }
            }
            // For loop untuk membuat JLabel untuk array daftarMatkul, mensetting tiap elemen, dan menambahkannya ke panelCenter 
            for (int i = 0; i < mahasiswa.getBanyakMatkul(); i++) {
                daftarMatkul = new JLabel[mahasiswa.getBanyakMatkul()];
                daftarMatkul[i] = new JLabel();
                // Mensetting text pada tiap elemen di daftarMatkul dengan isi array daftarMatkulString yang telah dibuat
                daftarMatkul[i].setText(daftarMatkulString[i]);
                daftarMatkul[i].setHorizontalAlignment(JLabel.CENTER);
                daftarMatkul[i].setFont(new Font("Century Gothic", Font.BOLD , 14));
                // Menambahkan tiap elemen pada daftarMatkul ke dalam panelCenter
                panelCenter.add(daftarMatkul[i]);
            }
        }
        
        // Menambahkan label ke panelCenter
        panelCenter.add(totalSKSLabel);
        panelCenter.add(hasilPengecekanLabel);
        
        // Memanggil method cekIRS pada mahasiswa untuk mengecek apakah ada masalah irs
        mahasiswa.cekIRS();
        // Jika tidak ada masalah irs
        if (mahasiswa.getBanyakMasalahIRS() == 0) {
            // Membuat "IRS tidak bermasalah." serta memasukkannya ke panelCenter
            JLabel hasilCekIRSLabel = new JLabel("IRS tidak bermasalah.");
            hasilCekIRSLabel.setHorizontalAlignment(JLabel.CENTER);
            hasilCekIRSLabel.setFont(new Font("Century Gothic", Font.BOLD , 14));
            panelCenter.add(hasilCekIRSLabel);
        }
        // Jika terdapat masalah irs
        else {
            // Membuat array masalahIRS dan diisi dengan masalah IRS mahasiswa
            String[] masalahIRS = new String[mahasiswa.getBanyakMasalahIRS()];
            masalahIRS = mahasiswa.getMasalahIRS();
            // Membuat array masalahIRSString dan daftarMasalahIRS
            String[] masalahIRSString = new String[mahasiswa.getBanyakMasalahIRS()];
            JLabel[] daftarMasalahIRS;
            // For loop untuk menambahkan array masalahIRSString dengan masalah irs sesuai format
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
                if (masalahIRS[i] != null) {
                    masalahIRSString[i] = String.format("%d. %s\n", i+1, masalahIRS[i]);
                }
            }
            // For loop untuk membuat JLabel untuk array daftarMasalahIRS, mensetting tiap elemen, dan menambahkannya ke panelCenter 
            for (int i = 0; i < mahasiswa.getBanyakMasalahIRS(); i++) {
                daftarMasalahIRS = new JLabel[mahasiswa.getBanyakMasalahIRS()];
                daftarMasalahIRS[i] = new JLabel();
                // Mensetting text pada tiap elemen daftarMasalahIRS dengan masalahIRSString
                daftarMasalahIRS[i].setText(masalahIRSString[i]);
                daftarMasalahIRS[i].setHorizontalAlignment(JLabel.CENTER);
                daftarMasalahIRS[i].setFont(new Font("Century Gothic", Font.BOLD, 14));
                // Menambahkan tiap elemen pada daftarMasalahIRS ke dalam panelCenter
                panelCenter.add(daftarMasalahIRS[i]);
            }
        }

        // Menambahkan panelSelesai ke panelCenter
        panelCenter.add(panelSelesai);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }

    
}
