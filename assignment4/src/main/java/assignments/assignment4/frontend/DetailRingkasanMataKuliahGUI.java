package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 500);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat label untuk Judul "Detail Ringkasan Mata Kuliah" dan men-setting label
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detai Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat main panel bernama panelCenter
        JPanel panelCenter = new JPanel();
        // Membuat panel untuk button
        JPanel panelSelesai = new JPanel();
        // Membuat label dan button
        JLabel namaLabel = new JLabel();
        JLabel kodeLabel = new JLabel();
        JLabel sksLabel = new JLabel();
        JLabel jumlahMhsLabel = new JLabel();
        JLabel kapasitasLabel = new JLabel();
        JLabel daftarMhsLabel = new JLabel();
        JButton selesaiButton = new JButton("Selesai");

        // Mencari banyak kolom untuk dimasukkan ke grid layout
        // Kolom yang tidak berubah jumlahnya adalah 8
        int kolom = 8;
        // Jika mata kuliah tidak memiliki mahasiswa maka kolom akan bertambah 1
        if (mataKuliah.getJumlahMahasiswa() == 0) {
            kolom += 1;
        }
        // Jika mata kuliah memiliki mahasiswa maka kolom akan bertambah dengan jumlah mahasiswa
        else {
            kolom += mataKuliah.getJumlahMahasiswa();
        }
        // Men-setting panelCenter dengan grid layout dengan kolom sesuai perhitungan diatas dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(kolom,1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan setiap label
        namaLabel.setText(String.format("Nama mata kuliah: %s", mataKuliah.getNama()));
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);

        kodeLabel.setText(String.format("Kode: %s", mataKuliah.getKode()));
        kodeLabel.setHorizontalAlignment(JLabel.CENTER);
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);

        sksLabel.setText(String.format("SKS: %s", mataKuliah.getSKS()));
        sksLabel.setHorizontalAlignment(JLabel.CENTER);
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);

        jumlahMhsLabel.setText(String.format("Jumlah mahasiswa: %d", mataKuliah.getJumlahMahasiswa()));
        jumlahMhsLabel.setHorizontalAlignment(JLabel.CENTER);
        jumlahMhsLabel.setFont(SistemAkademikGUI.fontGeneral);

        kapasitasLabel.setText(String.format("Kapasitas: %d", mataKuliah.getKapasitas()));
        kapasitasLabel.setHorizontalAlignment(JLabel.CENTER);
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);

        daftarMhsLabel.setText("Daftar Mahasiswa:");
        daftarMhsLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMhsLabel.setFont(SistemAkademikGUI.fontGeneral);

        // Men-setting tampilan selesaiButton
        selesaiButton.setFont(SistemAkademikGUI.fontGeneral);
        selesaiButton.setBackground(new Color(205, 227, 255));
        selesaiButton.setOpaque(true);
        // Menambahkan action listener untuk selesaiButton
        selesaiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });
        // Menambahkan button selesai ke panel selesai dan mengatur warna background
        panelSelesai.add(selesaiButton);
        panelSelesai.setBackground(HomeGUI.warnaKuning);

        // Menambahkan label ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(namaLabel);
        panelCenter.add(kodeLabel);
        panelCenter.add(sksLabel);
        panelCenter.add(jumlahMhsLabel);
        panelCenter.add(kapasitasLabel);
        panelCenter.add(daftarMhsLabel);

        // Menampilkan daftar mahasiswa
        // Jika mata kuliah tidak memiliki mahasiswa yang mengambil
        if (mataKuliah.getJumlahMahasiswa() <= 0) {
            // Membuat label "Belum ada mahasiswa yang mengambil mata kuliah ini." serta memasukkannya ke panelCenter
            JLabel daftarMahasiswaLabel = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            daftarMahasiswaLabel.setHorizontalAlignment(JLabel.CENTER);
            daftarMahasiswaLabel.setFont(new Font("Century Gothic", Font.BOLD , 14));
            panelCenter.add(daftarMahasiswaLabel);
        }
        // Jika ada mahasiswa yang mengambil mata kuliah
        else {
            // Membuat array listMahasiswa dan diisi dengan daftar mahasiswa yang mengambil matkul ini
            Mahasiswa[] listMahasiswa = new Mahasiswa[mataKuliah.getJumlahMahasiswa()];
            listMahasiswa = mataKuliah.getDaftarMahasiswa();
            // Membuat array daftarMahasiswaString dan daftarMhs
            String[] daftarMahasiswaString = new String[mataKuliah.getJumlahMahasiswa()];
            JLabel[] daftarMhs;
            // For loop untuk menambahkan array daftarMahasiswaString dengan nama mahasiswa sesuai format
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                if (listMahasiswa[i] != null) {
                    daftarMahasiswaString[i] = String.format("%d. %s\n", i+1,listMahasiswa[i].getNama());
                }
            }
            // For loop untuk membuat JLabel untuk array daftarMhs, mensetting tiap elemen, dan menambahkannya ke panelCenter 
            for (int i = 0; i < mataKuliah.getJumlahMahasiswa(); i++) {
                daftarMhs = new JLabel[mataKuliah.getJumlahMahasiswa()];
                daftarMhs[i] = new JLabel();
                // Mensetting text pada tiap elemen di daftarMhs dengan isi array daftarMahasiswaString yang telah dibuat
                daftarMhs[i].setText(daftarMahasiswaString[i]);
                daftarMhs[i].setHorizontalAlignment(JLabel.CENTER);
                daftarMhs[i].setFont(new Font("Century Gothic", Font.BOLD , 14));
                // Menambahkan tiap elemen pada daftarMhs ke dalam panelCenter
                panelCenter.add(daftarMhs[i]);
            }
        }

        // Menambahkan panelSelesai ke panelCenter
        panelCenter.add(panelSelesai);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }
}
