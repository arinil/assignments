package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {

    private JButton tambahMahasiswaButton;
    private JButton tambahMatkulButton;
    private JButton tambahIRSButton;
    private JButton hapusIRSButton;
    private JButton lihatRingkasanMahasiswaButton;
    private JButton lihatRingkasanMatkulButton;
    private JPanel panelTambahMahasiswa;
    private JPanel panelTambahMatkul;
    private JPanel panelTambahIRS;
    private JPanel panelHapusIRS;
    private JPanel panelLihatRingkasanMahasiswa;
    private JPanel panelLihatRingkasanMatkul;
    private JPanel panelHome;
    public static Color warna = new Color(202, 255, 218);
    public static Color warnaKuning = new Color(252, 251, 230);
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 500);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat lable untuk Judul "Selamat datang di Sistem Akademik" dan men-setting lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        
        // TODO: Implementasikan Halaman Home

        // Membuat panel dan button
        panelHome = new JPanel();
        panelTambahMahasiswa = new JPanel();
        panelTambahMatkul = new JPanel();
        panelTambahIRS = new JPanel();
        panelHapusIRS = new JPanel();
        panelLihatRingkasanMahasiswa = new JPanel();
        panelLihatRingkasanMatkul = new JPanel();
        tambahMahasiswaButton = new JButton("Tambah Mahasiswa");
        tambahMatkulButton = new JButton("Tambah Mata Kuliah");
        tambahIRSButton = new JButton("Tambah IRS");
        hapusIRSButton = new JButton("Hapus IRS");
        lihatRingkasanMahasiswaButton = new JButton("Lihat Ringkasan Mahasiswa");
        lihatRingkasanMatkulButton = new JButton("Lihat Ringkasan Mata Kuliah");

        // Menambahkan button ke panel yang sesuai dan men-set warna background
        panelTambahMahasiswa.add(tambahMahasiswaButton);
        panelTambahMahasiswa.setBackground(warnaKuning);
        panelTambahMatkul.add(tambahMatkulButton);
        panelTambahMatkul.setBackground(warnaKuning);
        panelTambahIRS.add(tambahIRSButton);
        panelTambahIRS.setBackground(warnaKuning);
        panelHapusIRS.add(hapusIRSButton);
        panelHapusIRS.setBackground(warnaKuning);
        panelLihatRingkasanMahasiswa.add(lihatRingkasanMahasiswaButton);
        panelLihatRingkasanMahasiswa.setBackground(warnaKuning);
        panelLihatRingkasanMatkul.add(lihatRingkasanMatkulButton);
        panelLihatRingkasanMatkul.setBackground(warnaKuning);

        // Men-set font dan warna background button tambah mahasiswa
        tambahMahasiswaButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahMahasiswaButton.setBackground(warna);
        tambahMahasiswaButton.setOpaque(true);
        // Menambahkan action listener untuk tambahMahasiswaButton
        tambahMahasiswaButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class TambahMahasiswaGUI dan men-setting visible dari panelCenter menjadi false
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-set font dan warna background button tambah mata kuliah
        tambahMatkulButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahMatkulButton.setBackground(warna);
        tambahMatkulButton.setOpaque(true);
        // Menambahkan action listener untuk tambahMatkulButton
        tambahMatkulButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class TambahMataKuliahGUI dan men-setting visible dari panelCenter menjadi false
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-set font dan warna background button tambah IRS
        tambahIRSButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahIRSButton.setBackground(warna);
        tambahIRSButton.setOpaque(true);
        // Menambahkan action listener untuk tambahIRSButton
        tambahIRSButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class TambahIRSGUI dan men-setting visible dari panelCenter menjadi false
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-set font dan warna background button hapus IRS
        hapusIRSButton.setFont(SistemAkademikGUI.fontGeneral);
        hapusIRSButton.setBackground(warna);
        hapusIRSButton.setOpaque(true);
        // Menambahkan action listener untuk hapusIRSButton
        hapusIRSButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HapusIRSGUI dan men-setting visible dari panelCenter menjadi false
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-set font dan warna background button lihat ringkasan mahasiswa
        lihatRingkasanMahasiswaButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMahasiswaButton.setBackground(warna);
        lihatRingkasanMahasiswaButton.setOpaque(true);
        // Menambahkan action listener untuk lihatRingkasanMahasiswaButton
        lihatRingkasanMahasiswaButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class RingkasanMahasiswaGUI dan men-setting visible dari panelCenter menjadi false
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-set font dan warna background button lihat ringkasan mata kuliah
        lihatRingkasanMatkulButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMatkulButton.setBackground(warna);
        lihatRingkasanMatkulButton.setOpaque(true);
        // Menambahkan action listener untuk lihatRingkasanMatkulButton
        lihatRingkasanMatkulButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class RingkasanMataKuliahGUI dan men-setting visible dari panelCenter menjadi false
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelHome.setVisible(false);
            }
        });

        // Men-setting panelHome dengan grid layout dengan kolom 7 dan baris 1 serta men-setting border dan warna backgrond
        panelHome.setLayout(new GridLayout(7,1));
        panelHome.setBorder(new EmptyBorder(70, 0, 70, 0));
        panelHome.setBackground(warnaKuning);

        // Menambahkan label dan panel ke panelHome
        panelHome.add(titleLabel);
        panelHome.add(panelTambahMahasiswa);
        panelHome.add(panelTambahMatkul);
        panelHome.add(panelTambahIRS);
        panelHome.add(panelHapusIRS);
        panelHome.add(panelLihatRingkasanMahasiswa);
        panelHome.add(panelLihatRingkasanMatkul);
        
        // Menambahkan panelHome ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelHome, BorderLayout.CENTER);
        frame.setVisible(true);
    }
}
