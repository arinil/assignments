package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    ArrayList<Mahasiswa> daftarMahasiswa;
    ArrayList<MataKuliah> daftarMataKuliah;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Menginisiasi daftarMahasiswa dan daftarMataKuliah
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(450, 450);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat lable untuk Judul "Ringkasan Mahasiswa" dan men-setting lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat array npmLong yang diisi dengan npm dari tiap mahasiswa pada daftarMahasiswa
        long[] npmLong = new long[daftarMahasiswa.size()];
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            if (daftarMahasiswa.get(i) != null) {
                long npm = daftarMahasiswa.get(i).getNpm();
                npmLong[i] = npm;
            }
        }
        // Implementasi sorting npm pada npmLonng
        long temp;
        for (int i = 0; i < npmLong.length; i++) {
            for (int j = i+1; j < npmLong.length; j++) {
                if (npmLong[i] > npmLong[j]) {
                    temp = npmLong[i];
                    npmLong[i] = npmLong[j];
                    npmLong[j] = temp;
                }
            }
        }
        // Convert array of long menjadi array of string dan disimpan pada array npmString
        String[] npmString = new String[npmLong.length];
        for (int i = 0; i < npmLong.length; i++) {
            npmString[i] = String.valueOf(npmLong[i]);
        }

        // Membuat main panel bernama panelCenter
        JPanel panelCenter = new JPanel();
        // Membuat panel untuk ComboBox dan Button
        JPanel panelCBNpm = new JPanel();
        JPanel panelLihat = new JPanel();
        JPanel panelKembali = new JPanel();
        // Membuat label, ComboBox, dan Button
        JLabel pilihNpmLabel = new JLabel("Pilih NPM");
        JComboBox npmCB = new JComboBox<>(npmString);
        JButton lihatButton = new JButton("Lihat");
        JButton kembaliButton = new JButton("Kembali");

        // Men-setting panelCenter dengan grid layout dengan kolom 5 dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(5,1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Menambahkan tiap ComboBox dan button ke masing-masing panel
        panelCBNpm.add(npmCB);
        panelCBNpm.setBackground(HomeGUI.warnaKuning);
        panelLihat.add(lihatButton);
        panelLihat.setBackground(HomeGUI.warnaKuning);
        panelKembali.add(kembaliButton);
        panelKembali.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan label
        pilihNpmLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNpmLabel.setHorizontalAlignment(JLabel.CENTER);

        // Mensetting tampilan lihatButton 
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(new Color(202, 255, 218));
        lihatButton.setOpaque(true);
        // Menambahkan action listener untuk lihatButton
        lihatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Jika hasil input dari comboBox kosong (tidak ada yang dipilih) maka akan ditampilkan message dialog "Mohon isi seluruh Field"
                if (npmCB.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                // Jika tidak kosong
                else {
                    // Membuat variable npm yang didapatkan dari selected item ComboBox
                    String npm = String.valueOf(npmCB.getSelectedItem());
                    // Membuat objek mahasiswa dari method getMahasiswa
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npm));
                    // Memanggil class DetailRingkasanMahasiswaGUI dan men-setting visible dari panelCenter menjadi false
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    panelCenter.setVisible(false);
                }
            }
        });

        // Men-setting tampilan kembaliButton
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setBackground(new Color(205, 227, 255));
        kembaliButton.setOpaque(true);
        // Menambahkan action listener untuk kembaliButton
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });

        // Menambahkan label dan panel ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(pilihNpmLabel);
        panelCenter.add(panelCBNpm);
        panelCenter.add(panelLihat);
        panelCenter.add(panelKembali);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);

    }

    // Uncomment method di bawah jika diperlukan
    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
