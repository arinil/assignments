package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    ArrayList<Mahasiswa> daftarMahasiswa;
    ArrayList<MataKuliah> daftarMataKuliah;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Ringkasan Mata Kuliah
        // Inisiasi daftarMahasiswa dan daftarMataKuliah
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(450, 450);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat lable untuk Judul "Ringkasan Mata Kuliah" dan men-setting lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat array namaMatkul yang diisi dengan nama-nama mata kuliah pada daftarMataKuliah
        String[] namaMatkul = new String[daftarMataKuliah.size()];
        for (int i = 0; i < daftarMataKuliah.size(); i++) {
            if (daftarMataKuliah.get(i) != null) {
                namaMatkul[i] = daftarMataKuliah.get(i).getNama();
            }
        }

         // Implementasi sorting nama matkul pada array namaMatkul
        String temp;
        for (int i = 0; i < namaMatkul.length; i++) {
            for (int j = i+1; j < namaMatkul.length; j++) {
                if (namaMatkul[i] != null && namaMatkul[j] != null && namaMatkul[j].compareTo(namaMatkul[i]) < 0) {
                    temp = namaMatkul[i];
                    namaMatkul[i] = namaMatkul[j];
                    namaMatkul[j] = temp;
                }
            }
        }

        // Membuat main panel bernama panelCenter
        JPanel panelCenter = new JPanel();
        // Membuat panel untuk ComboBox dan Button
        JPanel panelCBNamaMatkul = new JPanel();
        JPanel panelLihat= new JPanel();
        JPanel panelKembali = new JPanel();
        // Membuat label, ComboBox, dan Button
        JLabel pilihNamaLabel = new JLabel("Pilih Nama Matkul");
        JComboBox namaMatkulCB = new JComboBox<>(namaMatkul);
        JButton lihatButton = new JButton("Lihat");
        JButton kembaliButton = new JButton("Kembali");

        // Men-setting panelCenter dengan grid layout dengan kolom 5 dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(5,1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Menambahkan tiap ComboBox dan button ke masing-masing panel
        panelCBNamaMatkul.add(namaMatkulCB);
        panelCBNamaMatkul.setBackground(HomeGUI.warnaKuning);
        panelLihat.add(lihatButton);
        panelLihat.setBackground(HomeGUI.warnaKuning);
        panelKembali.add(kembaliButton);
        panelKembali.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan label
        pilihNamaLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNamaLabel.setHorizontalAlignment(JLabel.CENTER);

        // Mensetting tampilan lihatButton 
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(new Color(202, 255, 218));
        lihatButton.setOpaque(true);
        // Menambahkan action listener untuk lihatButton
        lihatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Jika hasil input dari comboBox kosong (tidak ada yang dipilih) maka akan ditampilkan message dialog "Mohon isi seluruh Field"
                if (namaMatkulCB.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                // Jika tidak kosong
                else {
                    // Membuat variable nama yang didapatkan dari selected item ComboBox
                    String nama = String.valueOf(namaMatkulCB.getSelectedItem());
                    // Membuat objek mata kuliah dari method getMataKuliah
                    MataKuliah mataKuliah = getMataKuliah(nama);
                    // Memanggil class DetailRingkasanMataKuliahGUI dan men-setting visible dari panelCenter menjadi false
                    new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                    panelCenter.setVisible(false);
                }
            }
        });

        // Men-setting tampilan kembaliButton
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setBackground(new Color(205, 227, 255));
        kembaliButton.setOpaque(true);
        // Menambahkan action listener untuk kembaliButton
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });

        // Menambahkan label dan panel ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(pilihNamaLabel);
        panelCenter.add(panelCBNamaMatkul);
        panelCenter.add(panelLihat);
        panelCenter.add(panelKembali);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }

    // Uncomment method di bawah jika diperlukan
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
}
