package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    JPanel panelCenter;
    JPanel panelCBNpm;
    JPanel panelCBNamaMatkul;
    JPanel panelTambahkan;
    JPanel panelKembali;
    JLabel pilihNpmLabel;
    JComboBox npmCB;
    JLabel pilihNamaLabel;
    JComboBox namaMatkulCB;
    JButton tambahkanButton;
    JButton kembaliButton;
    ArrayList<Mahasiswa> daftarMahasiswa;
    ArrayList<MataKuliah> daftarMataKuliah;

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // TODO: Implementasikan Tambah IRS
        // Inisiasi daftarMahasiswa dan daftarMataKuliah
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 500);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat lable untuk Judul "Tambah IRS" dan men-setting lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat array npmLong yang diisi dengan npm dari tiap mahasiswa pada daftarMahasiswa
        long[] npmLong = new long[daftarMahasiswa.size()];
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            if (daftarMahasiswa.get(i) != null) {
                long npm = daftarMahasiswa.get(i).getNpm();
                npmLong[i] = npm;
            }
        }
        // Implementasi sorting npm dari array npmLong
        long temp;
        for (int i = 0; i < npmLong.length; i++) {
            for (int j = i+1; j < npmLong.length; j++) {
                if (npmLong[i] > npmLong[j]) {
                    temp = npmLong[i];
                    npmLong[i] = npmLong[j];
                    npmLong[j] = temp;
                }
            }
        }
        // Convert array of long menjadi array of string dan disimpan pada array npmString
        String[] npmString = new String[npmLong.length];
        for (int i = 0; i < npmLong.length; i++) {
            npmString[i] = String.valueOf(npmLong[i]);
        }


        // Membuat array namaMatkul yang diisi dengan nama-nama mata kuliah pada daftarMataKuliah
        String[] namaMatkul = new String[daftarMataKuliah.size()];
        for (int i = 0; i < daftarMataKuliah.size(); i++) {
            if (daftarMataKuliah.get(i) != null) {
                namaMatkul[i] = daftarMataKuliah.get(i).getNama();
            }
        }
        // Implementasi sorting nama matkul pada array namaMatkul
        String temp2;
        for (int i = 0; i < namaMatkul.length; i++) {
            for (int j = i+1; j < namaMatkul.length; j++) {
                if (namaMatkul[j].compareTo(namaMatkul[i]) < 0) {
                    temp2 = namaMatkul[i];
                    namaMatkul[i] = namaMatkul[j];
                    namaMatkul[j] = temp2;
                }
            }
        }

        // Membuat main panel bernama panelCenter
        panelCenter = new JPanel();
        // Membuat panel untuk masing-masing comboBox dan Button
        panelCBNpm = new JPanel();
        panelCBNamaMatkul = new JPanel();
        panelTambahkan = new JPanel();
        panelKembali = new JPanel();
        // Membuat label "Pilih NPM"
        pilihNpmLabel = new JLabel("Pilih NPM");
        // Membuat ComboBox (drop down) untuk npm
        npmCB = new JComboBox<>(npmString);
        // Membuat label "Pilih Nama Matkul"
        pilihNamaLabel = new JLabel("Pilih Nama Matkul");
        // Membuat ComboBox (drop down) untuk nama mata kuliah
        namaMatkulCB = new JComboBox<>(namaMatkul);
        // Membuat button untuk tambahkan dan kembali
        tambahkanButton = new JButton("Tambahkan");
        kembaliButton = new JButton("Kembali");

        // Men-setting panelCenter dengan grid layout dengan kolom 7 dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(7,1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Menambahkan ComboBox dan Button sesuai dengan panel serta men-setting warna background
        panelCBNpm.add(npmCB);
        panelCBNpm.setBackground(HomeGUI.warnaKuning);
        panelCBNamaMatkul.add(namaMatkulCB);
        panelCBNamaMatkul.setBackground(HomeGUI.warnaKuning);
        panelTambahkan.add(tambahkanButton);
        panelTambahkan.setBackground(HomeGUI.warnaKuning);
        panelKembali.add(kembaliButton);
        panelKembali.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan pilihNpmLabel dan pilihNamaLabel
        pilihNpmLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNpmLabel.setHorizontalAlignment(JLabel.CENTER);
        pilihNamaLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNamaLabel.setHorizontalAlignment(JLabel.CENTER);

        // Mensetting tampilan tambahkanButton 
        tambahkanButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahkanButton.setBackground(new Color(202, 255, 218));
        tambahkanButton.setOpaque(true);
        // Menambahkan action listener untuk tambahkanButton
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Jika selected item dari ComboBox npmCB atau namaMatkulCB adalah null akan ditampilkan
                // Message dialog "Mohon isi seluruh field"
                if (npmCB.getSelectedItem() == null || namaMatkulCB.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                // Jika keduanya bukan null
                else {
                    // Membuat variable npm dan nama dari masing-masing selected item 
                    String npm = String.valueOf(npmCB.getSelectedItem());
                    String nama = String.valueOf(namaMatkulCB.getSelectedItem());
                    // Membuat objek mahasiswa dan mata kuliah dengan memanggil getMahasiswa dan getMataKuliah
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npm));
                    MataKuliah mataKuliah = getMataKuliah(nama);
                    // Menampilkan message dialog berupa hasil pemanggilan method addMatkul dari kelas Mahasiswa 
                    JOptionPane.showMessageDialog(frame, mahasiswa.addMatkul(mataKuliah));
                }
            }
        });

        // Men-setting tampilan kembaliButton
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setBackground(new Color(205, 227, 255));
        kembaliButton.setOpaque(true);
        // Menambahkan action listener untuk kembaliButton
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });

        // Menambahkan label dan panel ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(pilihNpmLabel);
        panelCenter.add(panelCBNpm);
        panelCenter.add(pilihNamaLabel);
        panelCenter.add(panelCBNamaMatkul);
        panelCenter.add(panelTambahkan);
        panelCenter.add(panelKembali);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }

    // Uncomment method di bawah jika diperlukan
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}
