package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    JPanel panelCenter;
    JPanel panelFieldNama;
    JPanel panelFieldNpm;
    JPanel panelTambahkan;
    JPanel panelKembali;
    JLabel namaLabel;
    JTextField fieldNama;
    JLabel npmLabel;
    JTextField fieldNpm;
    JButton tambahkanButton;
    JButton kembaliButton;
    ArrayList<Mahasiswa> daftarMahasiswa;
    ArrayList<MataKuliah> daftarMataKuliah;


    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // TODO: Implementasikan Tambah Mahasiswa
        // Menginisiasi daftarMahasiswa dan daftarMataKuliah
        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 500);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat lable untuk Judul "Tambah Mahasiswa" dan men-setting lable
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat main panel bernama panelCenter
        panelCenter = new JPanel();
        // Membuat label, text field, button, serta panel untuk masing-masing text field dan button
        panelFieldNama = new JPanel();
        panelFieldNpm = new JPanel();
        panelTambahkan = new JPanel();
        panelKembali = new JPanel();
        namaLabel = new JLabel("Nama:");
        fieldNama = new JTextField(17);
        npmLabel = new JLabel("NPM:");
        fieldNpm = new JTextField(17);
        tambahkanButton = new JButton("Tambahkan");
        kembaliButton = new JButton("Kembali");

        // Men-setting panelCenter dengan grid layout dengan kolom 7 dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(7,1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Menambahkan tiap text field dan button ke masing-masing panel
        panelFieldNama.add(fieldNama);
        panelFieldNama.setBackground(HomeGUI.warnaKuning);
        panelFieldNpm.add(fieldNpm);
        panelFieldNpm.setBackground(HomeGUI.warnaKuning);
        panelTambahkan.add(tambahkanButton);
        panelTambahkan.setBackground(HomeGUI.warnaKuning);
        panelKembali.add(kembaliButton);
        panelKembali.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan setiap label
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setHorizontalAlignment(JLabel.CENTER);

        // Mensetting tampilan tambahkanButton 
        tambahkanButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahkanButton.setBackground(new Color(202, 255, 218));
        tambahkanButton.setOpaque(true);
        // Menambahkan action listener untuk tambahkanButton
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Membuat variable untuk setiap input masing-masing dari text field
                String nama = fieldNama.getText();
                String npmString = fieldNpm.getText();
                // Jika hasil input dari textfield ada yang kosong maka akan ditampilkan message dialog "Mohon isi seluruh Field"
                if (nama.isEmpty() || npmString.isEmpty()) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                // Jika tidak ada yang kosong
                else {
                    // Membuat variable boolean tanda yang diassign dengan false
                    boolean tanda = false;
                    // Menjadikan npmString ke long
                    long npm = Long.parseLong(fieldNpm.getText());
                    // For loop untuk mengecek apakah input npm mahasiswa ada pada daftarMahasiswa
                    for (int i = 0; i < daftarMahasiswa.size(); i++) {
                        // Jika sudah ada, maka akan ditampilkan message box "NPM *npm* sudah pernah ditambahkan sebelumnya"
                        // dan seluruh text field akan di reset serta tanda boolean akan diassign menjad true
                        if (daftarMahasiswa.get(i).getNpm() == npm) {
                            JOptionPane.showMessageDialog(frame, String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm));
                            fieldNama.setText("");
                            fieldNpm.setText("");
                            tanda = true;
                        }
                    }
                    // Jika boolean tanda false (npm belum pernah ditamahkan)
                    if (!tanda) {
                        // Membuat objek mahasiswa dengan parameter hasil inputan 
                        Mahasiswa mahasiswa = new Mahasiswa(nama, npm);
                        // Menambahkan objek mahasiswa ke daftarMahasiswa
                        daftarMahasiswa.add(mahasiswa);
                        // Menampilkan message box "Mahasiswa *npm-nama* berhasil ditambahkan" dan seluruh text field akan di reset
                        JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %d-%s berhasil ditambahkan", npm, nama));
                        fieldNama.setText("");
                        fieldNpm.setText("");
                    }
                }
            }
        });

        // Men-setting tampilan kembaliButton
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setBackground(new Color(205, 227, 255));
        kembaliButton.setOpaque(true);
        // Menambahkan action listener untuk kembaliButton
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });

        // Menambahkan label dan panel ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(namaLabel);
        panelCenter.add(panelFieldNama);
        panelCenter.add(npmLabel);
        panelCenter.add(panelFieldNpm);
        panelCenter.add(panelTambahkan);
        panelCenter.add(panelKembali);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }
}
