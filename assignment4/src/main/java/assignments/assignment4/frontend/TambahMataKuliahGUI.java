package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // TODO: Implementasikan Tambah Mata Kuliah
        // Membuat title dan meng-set frame
        frame.setTitle("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setSize(500, 600);
        frame.setBackground(new Color(145, 145, 137));

        // Membuat label untuk Judul "Tambah Mata Kuliah" dan men-setting label
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Membuat main panel bernama panelCenter
        JPanel panelCenter = new JPanel();
        // Membuat label, text field, button, serta panel untuk masing-masing text field dan button
        JLabel kodeLabel = new JLabel("Kode Mata Kuliah:");
        JPanel panelFieldKode = new JPanel();
        JTextField fieldKode = new JTextField(17);
        JLabel namaMatkulLabel = new JLabel("Nama Mata Kuliah:");
        JPanel panelFieldNamaMatkul = new JPanel();
        JTextField fieldNamaMatkul = new JTextField(17);
        JLabel SKSLabel = new JLabel("SKS:");
        JPanel panelFieldSKS = new JPanel();
        JTextField fieldSKS = new JTextField(17);
        JLabel kapasitasLabel = new JLabel("Kapasitas:");
        JPanel panelFieldKapasitas = new JPanel();
        JTextField fieldKapasitas = new JTextField(17);
        JPanel panelTambahkan = new JPanel();
        JButton tambahkanButton = new JButton("Tambahkan");
        JPanel panelKembali = new JPanel();
        JButton kembaliButton = new JButton("Kembali");

        // Men-setting panelCenter dengan grid layout dengan kolom 11 dan baris 1 serta men-setting border dan warna backgrond
        panelCenter.setLayout(new GridLayout(11, 1));
        panelCenter.setBorder(new EmptyBorder(70,0,70,0));
        panelCenter.setBackground(HomeGUI.warnaKuning);

        // Men-setting tampilan setiap label
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        kodeLabel.setHorizontalAlignment(JLabel.CENTER);
        namaMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
        SKSLabel.setFont(SistemAkademikGUI.fontGeneral);
        SKSLabel.setHorizontalAlignment(JLabel.CENTER);
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        kapasitasLabel.setHorizontalAlignment(JLabel.CENTER);

        // Menambahkan tiap text field dan button ke masing-masing panel
        panelFieldKode.add(fieldKode);
        panelFieldKode.setBackground(HomeGUI.warnaKuning);
        panelFieldNamaMatkul.add(fieldNamaMatkul);
        panelFieldNamaMatkul.setBackground(HomeGUI.warnaKuning);
        panelFieldSKS.add(fieldSKS);
        panelFieldSKS.setBackground(HomeGUI.warnaKuning);
        panelFieldKapasitas.add(fieldKapasitas);
        panelFieldKapasitas.setBackground(HomeGUI.warnaKuning);
        panelTambahkan.add(tambahkanButton);
        panelTambahkan.setBackground(HomeGUI.warnaKuning);
        panelKembali.add(kembaliButton);
        panelKembali.setBackground(HomeGUI.warnaKuning);

        // Mensetting tampilan tambahkanButton 
        tambahkanButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahkanButton.setBackground(new Color(202, 255, 218));
        tambahkanButton.setOpaque(true);
        // Menambahkan action listener untuk tambahkanButton
        tambahkanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Membuat variable untuk setiap input masing-masing dari text field
                String kode = fieldKode.getText();
                String nama = fieldNamaMatkul.getText();
                String sksString = fieldSKS.getText();
                String kapasitasString = fieldKapasitas.getText();
                // Jika hasil input dari textfield ada yang kosong maka akan ditampilkan message dialog "Mohon isi seluruh Field"
                if (kode.isEmpty() || nama.isEmpty() || sksString.isEmpty() || kapasitasString.isEmpty()) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }
                // Jika tidak ada yang kosong
                else {
                    // Membuat variable boolean tanda yang diassign dengan false
                    boolean tanda = false;
                    // Menjadikan sks dan kapasitas ke integer
                    int sks = Integer.parseInt(sksString);
                    int kapasitas = Integer.parseInt(kapasitasString);
                    // For loop untuk mengecek apakah input nama mata kuliah ada pada daftarMataKuliah
                    for (int i = 0; i < daftarMataKuliah.size(); i++) {
                        // Jika sudah ada, maka akan ditampilkan message box "Mata Kuliah *nama matkul* sudah pernah ditambahkan sebelumnya"
                        // dan seluruh text field akan di reset serta tanda boolean akan diassign menjad true
                        if (daftarMataKuliah.get(i).getNama() == nama) {
                            JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama));
                            fieldKode.setText("");
                            fieldNamaMatkul.setText("");
                            fieldSKS.setText("");
                            fieldKapasitas.setText("");
                            tanda = true;
                        }
                    }
                    // Jika boolean tanda false (Matkul belum pernah ditamahkan)
                    if (!tanda) {
                        // Membuat objek mata kuliah dengan parameter hasil inputan 
                        MataKuliah mataKuliah = new MataKuliah(kode, nama, sks, kapasitas);
                        // daftarMataKuliah akan ditambahkan dengan objek mataKuliah 
                        daftarMataKuliah.add(mataKuliah);
                        // Menampilkan message box "Mata Kuliah *nama matkul* berhasil ditambahkan" dan seluruh text field akan di reset
                        JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan", nama));
                        fieldKode.setText("");
                        fieldNamaMatkul.setText("");
                        fieldSKS.setText("");
                        fieldKapasitas.setText("");
                    }
                }
            }
        });

        // Men-setting tampilan kembaliButton
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setBackground(new Color(205, 227, 255));
        kembaliButton.setOpaque(true);
        // Menambahkan action listener untuk kembaliButton
        kembaliButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                // Memanggil class HomeGUI dan men-setting visible dari panelCenter menjadi false
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
                panelCenter.setVisible(false);
            }
        });

        // Menambahkan label dan panel ke panelCenter
        panelCenter.add(titleLabel);
        panelCenter.add(kodeLabel);
        panelCenter.add(panelFieldKode);
        panelCenter.add(namaMatkulLabel);
        panelCenter.add(panelFieldNamaMatkul);
        panelCenter.add(SKSLabel);
        panelCenter.add(panelFieldSKS);
        panelCenter.add(kapasitasLabel);
        panelCenter.add(panelFieldKapasitas);
        panelCenter.add(panelTambahkan);
        panelCenter.add(panelKembali);

        // Menambahkan panelCenter ke dalam frame dan mensetting visible dai frame menjadi true
        frame.add(panelCenter);
        frame.setVisible(true);
    }
    
}
